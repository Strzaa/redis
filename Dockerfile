FROM redis:6.2.5
 
WORKDIR /

RUN mkdir adds

COPY . ./adds/

RUN chmod +x /adds/init.sh

ENTRYPOINT /bin/sh -c "redis-server" & sleep 2s && /adds/init.sh

