SET zdanie "To jest moje zdanie"

GET zdanie

RPUSH lista1 1
RPUSH lista1 2
RPUSH lista1 "koniec listy"

LRANGE lista1 0 -1

SADD zbior1 "pilka"
SADD zbior1 "siatka"
SADD zbior1 "reklamowka"

SMEMBERS zbior1

HSET dict1 "pilka" 50
HSET dict1 "reklamowka" 0.5
HSET dict2 1 "Adam"
HSET dict2 2 "Tomek"

HGETALL dict1
HGET dict1 "pilka"

GEOADD wspol1 13.361389 38.115556 "Miejsce nr 1"
GEOADD wspol1 2.352222 48.856614 "Miejsce nr 2"

