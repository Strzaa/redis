#!/bin/sh

redis-cli FLUSHALL
echo "OLD DATA DELETED"

cat /adds/script.txt | redis-cli  --pipe
echo "DATA ADDED"

redis-cli save
echo "DATA SAVED"

redis-cli shutdown
redis-server
echo "START DB"
